##Vanilla Industrial from Nick Chi

**Текстурпак**

1. [Скачать текстурпак](https://yadi.sk/d/j6cZj1h4gx0gEA). Будут все крафты со своими иконками.
2. [Скачать текстурпак](https://drive.google.com/open?id=1QZa6Jhvp18GHBOG_5vGMf53OOMWNVmx6).
3. [Скачать Minecraft Optifine](https://optifine.net/downloads). Очень надо для текстурпака.
4. [Скачать названия шапок](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/hat.txt) Оригинальные шапки из текстурпака.
---
**Таблички**

1. Если на воронку, сундук, печку повесить пустую табличку [см. пример](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_0.png), то предметы не будут приниматься.
2. Если повесить табличку с надписью [[INFO]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_1.png), то будет показываться из какого материала сделан предмет.
3. Для правильной работы машины нужно сначала поставить воронку на машину. На воронку прикрепить табличку. На табличке указать название машины. Потом на следующих 3 строках указать материалы которые будет перерабатывать машина.
4. Название машин: Энерго печь [[ПЕЧЬ]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_2.png), Энерго дробилка [[ДРОБИЛКА]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_3.png), [[ГЕНЕРАТОР]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_4.png), [КОМПРЕССОР], [ЭКСТРАКТОР], [ЦЕНТРИФУГА], [ДОМЕННАЯ ПЕЧЬ], [ОЧИСТИТЕЛЬ], [РЕПЛИКАТОР], [СКАНЕР], [УТИЛИЗАТОР], [ОБМЕН], [КОМПЬЮТЕР] или [[PC]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/pc.png).
 
---
**Энергия**

1. [Энергоблок](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/3.png). Энергитический блок нужен для работы механизмов и новых вещей.

---
**Провода**

1. [Изолирующий материал](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/1.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/1_opt.png).
Материал, служащий в основном для создания изолированных проводов.
2. [Изолированный провод](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/4.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/4_opt.png).
Необходим для сборки электросхем.
3. [Провод с двойной изоляцией](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/5.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/5_opt.png).
Весьма универсален и пользуется популярностью, у достаточно развитых игроков, которые накопили на МФЭ.
---
**Измельчённая руда**

1. [Измельчённая алмазная руда] При нагревании получается алмаз
2. [Измельчённая изумрудная руда] При нагревании получается изумруд
3. [Измельчённая золотая руда] При нагревании получается слиток золота.
4. [Измельчённая железная руда] При нагревании получается слиток железа
5. [Обсидиановая пыль] Используется для создания кристаллов памяти.
6. [Лазуритовая пыль] Используется для изготовления лазуротронового кристалла.
7. [Алмазная пыль] Вместе с красной пылью используется, для изготовления энергетической пыли, которая в свою очередь может быть сжата, в энергетический кристалл. Ни для чего больше не используется, невозможно превратить обратно в алмаз.
8. [Глиняная пыль](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/22.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/22_opt.png).
---
**Упакованная руда**

1. [Упакованный диорит](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/pak_1.png). При обработке в энерго печи получается железная руда.
Ингредиенты: 9 полированных диоритов.
2. [Упакованный андезит](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/pak_2.png). При обработке в энерго печи получается золотая руда.
Ингредиенты: 9 полированных андезитов.
3. [Упакованный гранит](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/pak_3.png). При обработке в энерго печи получается алмазная руда.
Ингредиенты: 5 полированных гранитов, 2 золотых слитка и 2 железных слитка.

---
**Машины**

1. [Железная печь](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/6.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/6_opt.png). Улучшенная печь. Но время горения топлива остаётся прежним.
Ингредиенты: Железный слиток + Печь 
2. [Энерго печь](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/7.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/7_opt.png). Работает на энергоблоках загружать их в сундук остальное грузить через воронку.
Ингредиенты: Красная пыль + Электросхема +Железная печь + Сундук
3. [Энерго дробилка](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/2.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/67.png). Работает на энергоблоках и позволяет дробить различные материалы, в том числе инструменты и броню кроме лопат.
Ингредиенты: Булыжник + Кремень + Основной корпус машины + Электросхема 
4. [Энерго доменная печь](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/59.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/59_opt.png). Используется для создания стали. 
Ингредиенты: Железный слиток + Основной корпус машины + Теплопровод + Электросхема + Сундук
5. [Энерго компрессор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/37.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/37_opt.png). Работает на энергоблоках. Это устройство, которое позволяет сжимать предметы и блоки. 
Ингредиенты: Камень + Основной корпус машины + Электросхема + Сундук
6. [Энерго центрифуга](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/61.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/61_opt.png). Позволяет извлекать из руд больше вещества и перерабатывать обеднённые твэлы.
Ингредиенты: Железный слиток + Катушка + Шахтёрский лазер + Расширенный корпус машины + Электромотор 
7. [Энерго экстрактор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/39.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/39_opt.png). Это устройство, способное извлекать вещества из раствора или сухой смеси.
Ингредиенты: Краник + Основной корпус машины + Электросхема + Сундук
8. [Энерго очиститель](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/32.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/32_opt.png). Позволяет извлекать из руд больше веществ. Работает на энергоблоках. Для работы нужно ведро воды.
Ингредиенты: Ведро + Железный слиток + Основной корпус машины + Электромотор + Электросхема + Сундук
9. [Энерго генератор материи](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/27.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/27_opt.png). Работает на энергоблокахи генерирует материю из утильсырья
Ингредиенты: Расширенный корпус машины + Светопыль + Улучшенная электросхема + Лазуротроновый кристалл 
10. [Энерго сканер](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/21.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/21_opt.png). Служит для получения шаблонов для репликатора. Внутри сканера должен быть кристалл памяти и энергоблоки 9 шт. Предмет сканирования подается через воронку.
Ингредиенты: Электромотор + Железный слиток + Укреплённое стекло + Электролампа + Улучшенная электросхема + Улучшенный корпус машины 
11. [Энерго утилизатор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/26.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/26_opt.png). Перерабатывает любой блок или предмет. В процессе переработки есть 12.5% шанс получения утильсырья, которое можно использовать как топливо иликак катализатор при получении материи. Утилизатор не выдает утильсырье при переработке стеклянных панелей, палок, строительных лесов и снежков.
Ингредиенты: Земля + Светопыль + Энерго компрессор + Железный слиток 
12. [Телепорт](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/44.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/44_opt.png). Устройство, с помощью которого можно мгновенно перемещаться на большие расстояния. Функция недоступна. Предмет, используемый в крафте высокотехнологичных вещей и машин.
Ингредиенты: Стекловолоконный провод + Улучшенная электросхема + Частотный связыватель + Расширенный корпус машины + Алмаз 
13. [Репликатор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/64.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/64_opt.png). Теперь чтобы создать какой-либо предмет из жидкой материи, вам необходимо поместить в репликатор жидкую материю и ксристал памяти с отсканированным объектом для дублирования.
Ингредиенты: Телепорт + Укреплённый камень + Укреплённое стекло + Трансформатор ВН + МФЭ 
14. [Ресурсообменник](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/18.png). Средство для бартера на сервере. Первая строка на табличке [ОБМЕН]. Вторая строка что принимает обменник. Третья что будет выдавать в замен, через пробел указыватся количество предметов. На вход всегда принимается одни предмет. На выход указывать через пробел. [см.пример](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/tab_5.png) 
Ингредиенты: Сундук + Красная пыль + Основной корпус машины 

---
**Генераторы**

41. [Генератор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/14.png). Перерабатывает один уголь в одну красную пыль.
Ингредиенты: Основной корпус машины + Железная печь + Аккумулятор 
42. [Геотермальный генератор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/17.png). Перерабатывает ведро лавы в блок красной пыли.
Ингредиенты: Стекло + Универсальная жидкостная капсула + Железный слиток + Генератор
---
**Рюкзаки**

1. [Рюкзак](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/10.png).
3. [Железный рюкзак](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/11.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/11_opt.png).
2. [Золотой рюкзак](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/12.png).
---
**Другие крафты**

1. [Аккумулятор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/15.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/15_opt.png).
2. [Улучшенный аккумулятор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/47.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/47_opt.png). Это улучшенное средство храненияи переноса энергии.
Ингредиенты: Измельчённая золотая руда + Сера + Железный слиток + Изолированный провод 
3. [Композитный слиток](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/29.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/29_opt.png). Представляет собой сочетание железа, золота и алмазов. Сплав может быть помещёнв компрессор для создания композита.
4. [Композит](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/30.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/30_opt.png). Используется при крафте многих высокотехнологичныхи прочных блоков и предметов. Можно получить если переработать композитный слиток в компрессоре.
5. [Майн](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/19.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/19_opt.png). Валюта для расчета на сервере.Может быть использованадля покупки различных товаров,или для покупки энергии. Скрафтить в режиме выживания невозможно. Майны можно только заработать или сделать копию в репликаторе (64 материи).
6. [Лампа](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/20.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/20_opt.png). Используется для крафта сканера.
7. [Лазер](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/60.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/60_opt.png). Инструмент для крафтов различных приборов.
8. [Лазуротроновый кристалл](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/62.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/62_opt.png). Используется для создания некоторых высокотехнологичных вещей.
9. [Материя](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/72_opt.png). Используется в репликаторе для создания предметов и блоков на основе кристаллов авмяти.
10. [Мотор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/31.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/31_opt.png). Используется при крафте некоторых механизмов.
11. [МФЭ](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/63.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/63_opt.png). Многофункциональный энергохранитель или МФЭЯ вляется хранилищем энергии.
12. [Катушка](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/8.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/8_opt.png). Используется при крафтенекоторых механизмовм.
13. [Кран](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/38.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/38_opt.png).
14. [Кристалл памяти](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/25.png). Используется для записи,обмена и хранения шаблонов.
15. [Необожжённый кристалл памяти](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/24.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/24_opt.png). Перед использованиемкристалл памяти необходимообжечь в энерго печи.
16. [Кучка серебренной пыли](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/71.png). Нужна для крафта серебренной пыли. Измельченная золотая руда помещается в центрифугу. На выходе 7 кусочков золота, угольная пыль и кучка серебренной пыли.
17. [Укреплённое стекло](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/46.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/46_opt.png). Гораздо прочнее обычного стекла. Используется для крафта некоторых высокотехнологичных вещей.
18. [Укреплённый камень](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/45.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/45_opt.png). Гораздо прочнее обычного камня. Используется для крафта некоторых высокотехнологичных вещей.
19. [Углеволокно](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/53.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/53_opt.png). Является одним из этапов производства углепластика. Других применений не имеет.
20. [Углеткань](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/54.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/54_opt.png). Является еще одним из этапов производства углепластика. Других применений не имеет.
21. [Углепластик](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/55.png). Является ингредиентом для многих высокотехнологичных вещей.
22. [Электросхема](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/34.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/34_opt.png). Нужна для крафта почти всех энерго приборов.
23. [Улучшенная электросхема](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/35.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/35_opt.png). Предмет, используемый в крафте высокотехнологичных вещей и машин. Других применений не имеет.
24. [Теплопровод](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/36.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/36_opt.png). Нужен для крафта механизмов, как-либо связанных с тепловой энергией. Других применений не имеет.
25. [Универсальная жидкостная капсула](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/16.png). С помощью капсул возможно взаимодействовать с содержащими жидкости и газы машинами и сооружениями
26. [Трансформатор низкого напряжения](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png).
27. [Трансформатор среднего напряжения](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/48.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/48_opt.png).
28. [Трансформатор высокого напряжения](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/49.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/49_opt.png).
29. [Серебренная пыль](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/41.png).
30. [Серная пыль](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png). Материал, служащий для создания улучшенного аккумулятора.
31. [Стекловолоконный провод](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/42.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/42_opt.png). Кабель, который может выдерживать высокое напряжение. Предмет, используемый в крафте высокотехнологичных вещей и машин.
32. [Основной корпус машины](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/28.png). Блок, выступающий в роли компонентадля создания различных устройств.
33. [Улучшенный корпус машины](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/033_opt.png). Блок, используемый для крафта некоторых высокотехнологичных вещей. Ингридиенты: В центре корпус, по углам сталь, по бокам композит и сверху снизу углепластик.
34. [Очищенная железная руда](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/33.png). Если переработать в доменной печи получиться слиток стали.
35. [Утильсырьё](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/73_opt.png).
36. [Плитка стали](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/52.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/52_opt.png). Нужна для крафта изделий из стали.
37. [Энергетическая пыль](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/43.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/43_opt.png). Используется для создания энергетических кристаллов.
38. [Энергетический кристалл](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/041_opt.png). Используется для создания некоторых высокотехнологичных вещей.
39. [Алмазный энерго меч](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/51.png). Меч из энергоблоков
40. [Энерго меч](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/50.png). Меч из энергоблоков
41. [Частотный связыватель](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/40.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/40_opt.png). Инструмент для крафтов и связи телепортов.
42. [Диоксид кремния](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/23.png) / [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/23_opt.png). Используется для создания кристалла памяти. Производится путем перерабатывания глиняной пыли в центрифуге.
43. [Крыло элитры](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/65.png). Используется для создания элитр.
Ингредиенты: укрепленное стекло 2 шт., укрепленный камень, углепластик 2 шт., сталь 2 шт., сера, переработанная мембрана фантома.
44. [Элитры](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/66.png).
Ингредиенты: крыло элитры 2 шт.,  золотое яблоко 2 шт., улучшенная схема, слеза гаста, серебро 2 шт., око эндера.
45. [Кусочек стали](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/x.png) /  [Optifine](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/68_opt.png).
46. [Переработанная мембрана фантома](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/69.png). Для получения нужно переработать мембрану в экстракторе.
47. [Слизь](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/70.png). Для получения нужно обработать переработанную мембрану фантома в центрифуге.
48. Кожа. Можно получить пережигая гнилую плоть.
---
**Компьютер**

1. [Материал для печатной платы](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/1.png)
Ингредиенты: Золотой слиток + Глина + Зеленый краситель
2. [Печатная плата](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/2.png) 
3. [Транзистор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/3.png)
Ингредиенты: Железный слиток 3 шт. + Кусочек золота 2 шт. Глина + Бумага + Красная пыль
4. [Пластина](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/4.png)
Ингредиенты: Железный кусочек 4 шт.
5. [Микрочип](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/5.png)
Ингредиенты: Железный кусочек 6 шт. + Красная пыль 2 шт. + Транзистор
6. [Базовая карта](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/6.png)
Ингредиенты: Печатная плата + Железный кусочек 3 шт. + Золотой кусочек
7. [АЛУ](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/7.png)
Ингредиенты: Микрочип + Транзистор 3шт. + Железный кусочек 4 шт. + Золотой кусочек
8. [Память](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/8.png)
Ингредиенты: Микрочип 2шт. + Железный кусочек + Печатная плата
9. [Видеокарта](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/9.png)
Ингредиенты: Микрочип + АЛУ + Память + Базовая карта
10. [Управляющий автомат](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/10.png)
Ингредиенты: Золотой кусочек 4 шт. + Красная пыль + Часы + Транзистор 3 шт.
11. [Центральный процессорт](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/11.png)
Ингредиенты: Железный кусочек 4 шт. + Красная пыль + Управляющий автомат + Микрочип 2 шт. + АЛУ
12. [Жёсткий диск](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/12.png)
Ингредиенты: Микрочип 2 шт. + Печатная плата + Пластина 3 шт. + Железный слиток 2 шт. + Поршень
13. [Дискета](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/13.png)
Ингредиенты: Бумага 3 шт. + Пластина + Железный кусочек 4 шт. + Рычаг
На дискете можно написать программу на lua и запустить в компьютере на выполнение.
Результат программы появиться ввиде сообщения пользователю. Дискету нужно загружать через загрузочную воронку.
Пример кода на lua:
print("Hello World!")
14. [Системный блок](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/14.png)
Игредиенты: Железный слиток 4 шт. + Микрочип + Железная решётка 2 шт. + Сундук + Печатная плата
Чтобы компьютер правильно работал необходимо поместить в системный блок: процессор, память, жёсткий диск, видеокарту, энергоблок.
Так же нужно повесить табличку и подписать ее как [КОМПЬЮТЕР] или [[PC]](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/pc.png) и материал из которого сделана дискета: WRITTEN_BOOK или WRITABLE_BOOK.
На выполнение одной программы расходуется один энергоблок.
15. [Красная плата](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/15.png) Принимает и выдает сигналы редстоуна рядом с ситемным блоком.
Игредиенты: Красный факел + Микрочип + Базовая карта
принимает и выдает сигналы редстоуна
Функции на lua: 
reds.set(face, flag ) установить сигнал
reds.get(face) получить сигнал
face = 0,1,2,3 направления вокруг сундука 0-север, 1-восток, 2-юг, 4-запад
flag=true/false вкл/выкл
Пример кода:
reds.set(0, false) ---выключить кнопку справа
r=reds.get(2) ---принять сигнал слева
15. [Анализатор](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/16.png) Используется для получения информации о блоках и для кравта строительной карты.
Игредиенты: Красный факел + Транзистор + Печатная плата + Золотой кусочек 2 шт.
17. [Строительная карта](https://bitbucket.org/NickChi/vanillaindustrialdocs/src/master/img/comp/17.png) Используется для строительства. Можно делать копию постройки. Только необходимо положить в ресурсный сундук все необходимые блоки.
Игредиенты: 1-я в верстаке линия Азмаз Анализатор Алмаз, 2-линия Микрочик Базовая карта Микрочип, 3-линия Память Компаратор Память
Функции на lua:
str=build.copy(n) копировать периметр на n-блоков по сторонам от системного блока, вверх на n-1 блоков, 1 блок вниз. Максимальное значение n=15.
На выходе строка с копией, которую можно сохранить в файл на жестком диске.
build.paste(face, str) строить постройку.
face = 0,1,2,3 направление возле системного блока где искать ресурсный сундук. 0-север, 1-восток, 2-юг, 4-запад
str строка с копией территории. Копию можно считать с жёсткого диска.
---
**Дополнительные команды**

1. /ch <message>  - всплывает сообщение над головой игрока, через время исчезает. Можно писать цветные сообщения.

---
**Дополнительные возможности**

1. Можно биркой подписать стойку для брони. Чтобы надпись пропала надо подписать пустой биркой.
